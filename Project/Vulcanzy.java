package Project;

import java.awt.*;
import java.awt.event.*;
import Project.Inventory;

public class Vulcanzy implements ActionListener {
    public Frame frame;
    public Label label;
    public Button Clubs;
    public Button Organisers;
    public Button Locations;
    public Button Events;
    public Button Volunteers;
    public Button Participants;

    public Inventory clubs;
    public Inventory organisers;
    public Inventory locations;
    public Inventory events;
    public Inventory volunteers;
    public Inventory participants;

    Vulcanzy() {
        frame = new Frame();

        label = new Label("Vulcanzy Event Management");

        Clubs = new Button("Clubs");
        Organisers = new Button("Organisers");
        Locations = new Button("Locations");
        Events = new Button("Events");
        Volunteers = new Button("Volunteers");
        Participants = new Button("Participants");

        frame.add(label);
        frame.add(Clubs);
        frame.add(Organisers);
        frame.add(Locations);
        frame.add(Events);
        frame.add(Volunteers);
        frame.add(Participants);

        Clubs.addActionListener(this);
        Organisers.addActionListener(this);
        Locations.addActionListener(this);
        Events.addActionListener(this);
        Volunteers.addActionListener(this);
        Participants.addActionListener(this);

        frame.setVisible(true);
        frame.setSize(200, 200);
        frame.setLayout(new FlowLayout());

    }

    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        switch (action) {
            case "Clubs":
                clubs = new Inventory("Clubs");
                break;
            case "Organisers":
                organisers = new Inventory("Organisers");
                break;
            case "Locations":
                locations = new Inventory("Locations");
                break;
            case "Events":
                events = new Inventory("Events");
                break;
            case "Volunteers":
                volunteers = new Inventory("Volunteers");
                break;
            case "Participants":
                participants = new Inventory("Particpants");
                break;
        }
    }

    public static void main(String[] args) {
        Vulcanzy s = new Vulcanzy();
    }
}