package Project;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Inventory implements ActionListener {

    private int size;
    private Frame frame;

    private Label nameLabel;
    private Label idLabel;
    private Label descLabel;

    private ArrayList<Label> nameLabels;
    private ArrayList<Label> idLabels;
    private ArrayList<Label> descLabels;

    private Button addButton;
    private Button updButton;
    private Button delButton;

    private TextField nameInput;
    private TextField idInput;
    private TextField descInput;

    Inventory(String Table) {
        this.size = 0;
        this.frame = new Frame();

        this.nameLabel = new Label(Table);
        this.idLabel = new Label("id");
        this.descLabel = new Label("desc");

        this.nameLabels = new ArrayList<>();
        this.idLabels = new ArrayList<>();
        this.descLabels = new ArrayList<>();

        this.addButton = new Button("Add");
        this.updButton = new Button("Update");
        this.delButton = new Button("Delete");

        this.addButton.addActionListener(this);
        this.updButton.addActionListener(this);
        this.delButton.addActionListener(this);

        this.nameInput = new TextField();
        this.idInput = new TextField();
        this.descInput = new TextField();

        this.makeGUI();
    }

    public void makeGUI() {
        this.frame.removeAll();

        this.frame.setLayout(new GridLayout(this.size + 3, 3));
        this.frame.setVisible(true);
        this.frame.setSize(200, 50 * (this.size + 3));

        this.frame.add(nameLabel);
        this.frame.add(idLabel);
        this.frame.add(descLabel);

        for (int i = 0; i < this.size; i++) {
            this.frame.add(this.nameLabels.get(i));
            this.frame.add(this.idLabels.get(i));
            this.frame.add(this.descLabels.get(i));
        }

        this.frame.add(updButton);
        this.frame.add(addButton);
        this.frame.add(delButton);

        this.frame.add(nameInput);
        this.frame.add(idInput);
        this.frame.add(descInput);
    }

    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();

        switch (action) {
            case "Add":
                this.addItem(this.nameInput.getText(), this.idInput.getText(), this.descInput.getText());
                break;
            case "Update":
                this.updItem(this.nameInput.getText(), this.idInput.getText(), this.descInput.getText());
                break;
            default:
                this.delItem(this.nameInput.getText());
                break;
        }
        this.nameInput.setText("");
        this.idInput.setText("");
        this.descInput.setText("");
        this.makeGUI();
    }

    public void addItem(String name, String id, String desc) {
        this.nameLabels.add(new Label(name));
        this.idLabels.add(new Label(id));
        this.descLabels.add(new Label(desc));
        this.size++;
    }

    public void updItem(String name, String id, String desc) {
        int index = this.getIndex(name);
        if (index != -1) {
            this.nameLabels.remove(index);
            this.idLabels.remove(index);
            this.descLabels.remove(index);
            this.nameLabels.add(index, new Label(name));
            this.idLabels.add(index, new Label(id));
            this.descLabels.add(index, new Label(desc));
        }
    }

    public void delItem(String name) {
        int index = this.getIndex(name);
        if (index != -1) {
            this.nameLabels.remove(index);
            this.idLabels.remove(index);
            this.descLabels.remove(index);
            this.size--;
        }
    }

    public int getIndex(String name) {
        for (int i = 0; i < this.size; i++) {
            if (this.nameLabels.get(i).getText() == name) {
                return i;
            }
        }
        return -1;
    }
}